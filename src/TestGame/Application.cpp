#include "guif/Screen.hpp"
#include "claw/application/AbstractApp.hpp"
#include "claw/application/DebugOverlay.hpp"
#include "claw/sound/mixer/effect/EffectVolumeShift.hpp"
#include "claw/sound/mixer/source/AudioPosition.hpp"
#include "claw/vfs/NativeMount.hpp"
#include "claw/vfs/ZMount.hpp"

#include "TestGame/Application.hpp"
#include <iostream>

namespace TestGame
{

    TestGameApplication::TestGameApplication() : m_mainWindow( new Guif::Screen() )
    {

    }

    TestGameApplication::~TestGameApplication()
    {
    }

    void TestGameApplication::OnStartup()
    {
		//create required assets

		CreateAssetDict();
		CreateTextDict();
		CreateRegistry();
		Claw::DebugOverlay::Create();
		Claw::Vfs::Mount("/", std::make_shared<Claw::NativeMount>("../../res/"));

		m_mainWindow->Lua().Load("menu.lua");

    }

    void TestGameApplication::OnShutdown()
    {
    }

    void TestGameApplication::OnUpdate( float dt )
    {
		m_mainWindow->Update(dt);
    }

    void TestGameApplication::OnRender( Claw::Surface* target )
    {
		target->Clear( 0 );
		m_mainWindow->Render(target);
    }

    void TestGameApplication::OnKeyPress( Claw::KeyCode code )
    {
		m_mainWindow->OnKeyDown(code);
    }

    void TestGameApplication::OnKeyRelease( Claw::KeyCode code )
    {
		m_mainWindow->OnKeyUp(code);
    }

    void TestGameApplication::OnTouchDown( int x, int y, int button )
    {
		m_mainWindow->OnTouchDown( x, y, button );
    }

    void TestGameApplication::OnTouchUp( int x, int y, int button )
    {
		m_mainWindow->OnTouchUp(x, y, button);
    }

    void TestGameApplication::OnTouchMove( int x, int y, int button )
    {
		m_mainWindow->OnTouchMove(x, y, button);
    }

}

CLAW_DEFINE_APPLICATION( new TestGame::TestGameApplication, "TestGame" );
