varying lowp vec4 vColor;

void main(void)
{
    lowp vec4 t = GetTextureBias( vTex, -0.35 );
    gl_FragColor = t * vColor;
}
