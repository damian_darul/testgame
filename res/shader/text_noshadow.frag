varying lowp vec4 vColor;
uniform lowp vec3 col1;
uniform lowp vec3 col2;

void main(void)
{
    lowp vec4 t = GetTextureBias( vTex, -0.35 );

    lowp float l = t.r;
    lowp float s = t.g;
    lowp float m = t.b;
    lowp float a = t.a;

    gl_FragColor = vec4( mix( vec3( 0.0, 0.0, 0.0 ), mix( vec3( 1.0, 1.0, 1.0 ), mix( col1, col2, m ), s ), l ), mix( 0.0, a, l ) ) * vColor;
}
