local laser = {}
local timeCounter = 0
local timeout = 0.08
local dltMove = 20
local alienShips = nil

function laser:Init(ships)
	alienShips = ships
end

function laser:Create( root, x, y )
	
	if	laser["object"] == nil then
		laser["x"] = x + 20
		laser["y"] = y		
		laser["object"] = root:InsertControl()	
		laser["object"]:SetPos(laser["x"], laser["y"])
		laser["object"]:AddTree("blaster.xml")
	end

end

function laser:Move( dt )
	
	if	laser["object"] ~= nil then
		
		timeCounter = timeCounter + dt
		
		if timeCounter > timeout then		
			
			laser["y"] = laser["y"] - dltMove		
			laser["object"]:SetPos(laser["x"], laser["y"])
			
			local closestShipPosY = 200
			
			if laser["y"] < closestShipPosY then
				
                result = laser:CheckIfHitTarget(laser["x"], laser["y"])	
                
				if ( result == true ) or ( laser["y"] < 10 ) then
					laser["object"]:Remove()
					laser["object"] = nil
				end		
			end
			
			timeCounter = 0			
		end
		
	end
end

function laser:CheckIfHitTarget( x, y )
	
	local result = alienShips:CheckIfHitTarget(x, y)
	
	if true == result then
		print("TANGO DOWN!") 
	end
	
	return result
	
end



return laser