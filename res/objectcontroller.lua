local spaceShip = {}
local dltMove = 20

function spaceShip:Create(root, x, y)
	
	spaceShip["x"]  = x 
	spaceShip["y"]  = y 
	spaceShip["spaceShip"] = root:InsertControl()	
	spaceShip["spaceShip"]:SetPos(x, y)
	spaceShip["spaceShip"]:AddTree("image_2.xml", nil, {fileName = "spaceInvaders_ship.png@linear"})
end

function spaceShip:MoveLeft()
	spaceShip["x"] = spaceShip["x"] + dltMove
	spaceShip["spaceShip"]:SetPos(spaceShip["x"], spaceShip["y"])
end

function spaceShip:MoveRight()
	spaceShip["x"] = spaceShip["x"] - dltMove
	spaceShip["spaceShip"]:SetPos(spaceShip["x"], spaceShip["y"])
end

function spaceShip:GetPosX()
	return spaceShip["x"]
end

function spaceShip:GetPosY()
	return spaceShip["y"]
end

function spaceShip:GetPosition()
	return spaceShip["x"], spaceShip["y"]
end


return spaceShip