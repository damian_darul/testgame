shipUser   = load( "objectcontroller.lua" )
shipAliens = load( "shipscontroler.lua" ) -- ktoś tu nie umie w angielski
laser 	   = load( "shiplasercontroler.lua" ) -- tu też
points	   = load( "pointscounter.lua" )

keyRight    = 1
keyLeft     = 2
keySpacebar = 27 

local screenStates = 
{ 
	menuWindow    = 0,
	startGame     = 1,
	selectLevel   = 2,
	inviteFriends = 3,
	goBack		  = 4,
	levelOne	  = 5,
	levelTwo	  = 6,
	levelThree	  = 7,
}

local gameLevel = 
{ 
	levelOne    = 1,
	levelTwo    = 2,
	levelThree  = 3,
}

local screenViewState = screenStates.menuWindow

----- END -----


function Screen:OnTouchDown(  x, y, id  ) 
	
	local item = screen:GetTouchItem(  x, y  )
	
	if item then 
		
		local name = item:GetName()
		
		if name == "StartGame" then
			ChangeScreenView( screenStates.startGame )		
		elseif name == "SelectLevel" then
			ChangeScreenView( screenStates.selectLevel )
		elseif name == "InviteFriends" then
			ChangeScreenView( screenStates.inviteFriends )
		elseif name == "LevelOne" then
			ChangeScreenView( screenStates.levelOne )
		elseif name == "LevelTwo" then
			ChangeScreenView( screenStates.levelTwo )
		elseif name == "LevelThree" then
			ChangeScreenView( screenStates.levelThree )
		elseif name == "GoBack" then
			ChangeScreenView( screenStates.goBack )	
					
		end
	
	end

end
function Screen:OnTouchUp(  x, y, id  ) end
function Screen:OnTouchMove(  x, y, id  ) end

function Screen:OnKeyDown(  code  ) 

	if code == keyRight then -- Confused face?
		shipUser:MoveRight()
	elseif code == keyLeft then 
		shipUser:MoveLeft()
	elseif code == keySpacebar then 
    
		local posx = shipUser:GetPosX()
		local posy = shipUser:GetPosY()
		laser:Create( root, posx, posy ) 
        
	end
end

function Screen:OnKeyUp(  code  ) end

function Screen:Update(  dt  ) 
	
	if( screenStates.startGame == screenViewState ) then
		laser:Move(  dt  )
		shipAliens:Update(  dt  )
	end
	
end

function ChangeScreenView(  state  )
		
	if state == screenStates.menuWindow    then		
		ShowMainScreen()				
	elseif state == screenStates.startGame     then			
		StartGame( gameLevel.levelOne )		
	elseif state == screenStates.selectLevel   then
		SelectLevel()
	elseif state == screenStates.goBack then
		ShowMainScreen()				
	elseif state == screenStates.levelOne then
		StartGame( gameLevel.levelOne )
		state = screenStates.startGame
	elseif state == screenStates.levelTwo then
		StartGame( gameLevel.levelTwo )
		state = screenStates.startGame			
	elseif state == screenStates.levelThree then
		StartGame( gameLevel.levelThree )
		state = screenStates.startGame
	end	
			
		screenViewState = state	
end

function ShowMainScreen()
	
	if( nil ~= root ) then
		root:Remove()
		root = nil
	end
	
	screen:GetControl(  "/"  ):InsertFromXml(  "menu.xml"  )
	root         = screen:GetControl(  "/root"  )
	menuWindow   = root:GetControl( "gamemenu" )
	local sw, sh = screen:GetSize() 
	local scale  = sw/1280          
	
	root:SetMatrix( scale, 0, 0, scale )	
end

function StartGame(  level  )
	
	menuWindow:Remove() 
	menuWindow = nil	
	
	local sw, sh = screen:GetSize() 
	local scale  = sw/1920          
	
	points:Create(  root, scale, sw, sh  )
	shipUser:Create(  root, 360, 600  )
	shipAliens:Create(  root, points, level, scale, 70, 50  )
	laser:Init(  shipAliens  )	
end

function SelectLevel()
	
	menuWindow:Remove()
	menuWindow = nil
	root:Remove()
	root = nil

	screen:GetControl(  "/"  ):InsertFromXml(  "selectmenu.xml"  )
	root       = screen:GetControl(  "/root"  )
	menuWindow = root:GetControl( "selectmenu" )
	local sw, sh = screen:GetSize() 
	local scale  = sw/1280          
	root:SetMatrix( scale, 0, 0, scale )	

end

ChangeScreenView( screenViewState )

